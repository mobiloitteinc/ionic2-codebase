var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, MenuController, Events } from 'ionic-angular';
import { MychatPage } from '../mychat/mychat';
import { AngularFireDatabase } from 'angularfire2/database';
import { UsersproviderProvider } from '../../providers/usersprovider/usersprovider';
var MyqueuePage = (function () {
    function MyqueuePage(navCtrl, menuCtrl, events, db, up) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.events = events;
        this.db = db;
        this.up = up;
        this.chatsarr = [];
        this.myqueuearr = [];
        this.usertabledata = [];
        this.myqueueshowarr = [];
        this.temparr = [];
        this.filteredusers = [];
        this.up.getallusers().then(function (res) {
            _this.filteredusers = res;
            console.log('this.filteredusers :: ' + JSON.stringify(_this.filteredusers));
            _this.temparr = res;
        });
    }
    MyqueuePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.menuCtrl.enable(true);
        this.username = localStorage.getItem('chatfirstName');
        if (localStorage.getItem('chatfirsttoken') == null || localStorage.getItem('chatfirsttoken') == undefined || localStorage.getItem('chatfirsttoken') == '') {
            this.events.publish('user:session');
        }
        this.up.getUid()
            .then(function (uid) {
            _this.useridkey = uid;
            _this.chatdata = _this.db.list("/chats");
            _this.chatsarr = [];
            _this.myqueueshowarr = [];
            _this.myqueuearr = [];
            _this.chatdata.forEach(function (element) {
                _this.chatsarr = element;
                console.log('this.chatsarr :: ' + JSON.stringify(_this.chatsarr));
                var _loop_1 = function (i) {
                    if (_this.chatsarr[i].$key.indexOf(_this.useridkey) >= 0) {
                        _this.myqueueshowarr.push(Object.keys(_this.chatsarr[i]).map(function (key) { return _this.chatsarr[i][key]; }));
                    }
                };
                for (var i = 0; i < _this.chatsarr.length; i++) {
                    _loop_1(i);
                }
                console.log('this.myqueueshowarr arr ::: ' + JSON.stringify(_this.myqueueshowarr));
                var temp = [];
                _this.myqueuearr = _this.myqueueshowarr.map(function (item) { return item[0]; });
                _this.myqueuearr = _this.myqueuearr.filter(function (x, i) {
                    if (temp.indexOf(x.userid) < 0) {
                        temp.push(x.userid);
                        return true;
                    }
                    return false;
                });
                console.log('this.myqueuearr arr ::: ' + JSON.stringify(_this.myqueuearr));
            });
        });
    };
    MyqueuePage.prototype.chatkey = function (userid) {
        // this.db.list('/users')
        //     .subscribe(data => {
        //         this.usertabledata = data
        //         for(let i=0; i<this.usertabledata.length; i++){
        //               if(this.usertabledata[i].email == email){
        //                 this.param = {interlocutor: this.usertabledata[i].$key, emailid: email};
        //             }
        //         }
        //     })
        this.param = { interlocutor: userid };
        this.navCtrl.push(MychatPage, this.param);
    };
    return MyqueuePage;
}());
MyqueuePage = __decorate([
    Component({
        selector: 'page-myqueue',
        templateUrl: 'myqueue.html'
    }),
    __metadata("design:paramtypes", [NavController, MenuController,
        Events, AngularFireDatabase, UsersproviderProvider])
], MyqueuePage);
export { MyqueuePage };
//# sourceMappingURL=myqueue.js.map