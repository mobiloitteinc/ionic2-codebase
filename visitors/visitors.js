var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, MenuController, Events } from 'ionic-angular';
import { MychatPage } from '../mychat/mychat';
import { CommondataProvider } from '../../providers/commondata/commondata';
var VisitorsPage = (function () {
    function VisitorsPage(navCtrl, menuCtrl, events, commondata) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.events = events;
        this.commondata = commondata;
    }
    VisitorsPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        if (localStorage.getItem('chatfirsttoken') == null || localStorage.getItem('chatfirsttoken') == undefined) {
            this.events.publish('user:session');
        }
    };
    VisitorsPage.prototype.chat = function () {
        this.navCtrl.push(MychatPage);
    };
    VisitorsPage.prototype.chatbutton = function () {
        this.events.publish('user:chat');
    };
    return VisitorsPage;
}());
VisitorsPage = __decorate([
    Component({
        selector: 'page-visitors',
        templateUrl: 'visitors.html'
    }),
    __metadata("design:paramtypes", [NavController, MenuController, Events, CommondataProvider])
], VisitorsPage);
export { VisitorsPage };
//# sourceMappingURL=visitors.js.map