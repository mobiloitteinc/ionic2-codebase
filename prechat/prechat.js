var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { CommondataProvider } from '../../providers/commondata/commondata';
import { Http, Headers } from '@angular/http';
import { MyqueuePage } from '../myqueue/myqueue';
/**
 * Generated class for the PrechatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var apiUrl = 'http://ec2-52-221-54-107.ap-southeast-1.compute.amazonaws.com/PROJECTS/Firebase/website/public/api/';
var PrechatPage = (function () {
    function PrechatPage(navCtrl, navParams, loadingCtrl, commondata, http, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.commondata = commondata;
        this.http = http;
        this.events = events;
        this.disableemail = true;
        this.disablenumber = true;
        this.namerequire = false;
        this.emailcheck = false;
        this.emailrequire = false;
        this.phonecheck = false;
        this.phonerequire = false;
        this.onlinecheck = false;
        this.prechatcheck = false;
    }
    PrechatPage.prototype.ionViewDidLoad = function () {
        localStorage.setItem('emailrequirecheck', 'false');
        if (localStorage.getItem('chatfirsttoken') == null || localStorage.getItem('chatfirsttoken') == undefined) {
            this.events.publish('user:session');
        }
        this.getprechat();
        this.Adminid = localStorage.getItem('companyid');
    };
    PrechatPage.prototype.getprechat = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('chatfirsttoken'));
            var body = JSON.stringify({
                userId: localStorage.getItem('chatfirstid'),
            });
            console.log(body);
            _this.http.post(apiUrl + 'prechat-info', body, { headers: headers })
                .subscribe(function (res) {
                _this.data = res.json();
                console.log('get pre chat data' + JSON.stringify(_this.data));
                if (_this.data.data.nameRequired == '1' || _this.data.data.nameRequired == true) {
                    _this.namerequire = true;
                }
                if (_this.data.data.email == '1' || _this.data.data.email == true) {
                    _this.emailcheck = true;
                    _this.disableemail = false;
                }
                if (_this.data.data.emailRequired == '1' || _this.data.data.emailRequired == true) {
                    _this.emailrequire = true;
                    localStorage.setItem('emailrequirecheck', _this.data.data.emailRequired);
                }
                else {
                    localStorage.setItem('emailrequirecheck', 'false');
                }
                if (_this.data.data.phone == '1' || _this.data.data.phone == true) {
                    _this.phonecheck = true;
                    _this.disablenumber = false;
                }
                if (_this.data.data.phoneRequired == '1' || _this.data.data.phoneRequired == true) {
                    _this.phonerequire = true;
                    localStorage.setItem('phonerequirecheck', _this.data.data.phoneRequired);
                }
                else {
                    localStorage.setItem('phonerequirecheck', 'false');
                }
                if (_this.data.data.formAutoFillRequest == '1' || _this.data.data.formAutoFillRequest == true) {
                    _this.onlinecheck = true;
                }
                if (_this.data.data.formEnable == '1' || _this.data.data.formEnable == true) {
                    _this.prechatcheck = true;
                }
                resolve(res.json());
            }, function (err) {
                _this.errdata = err.json();
                console.log('err' + JSON.stringify(err));
                if (_this.errdata.error == 'token_invalid') {
                    _this.events.publish('user:session');
                }
            });
        });
    };
    PrechatPage.prototype.emailvalue = function (val) {
        if (val == true) {
            this.disableemail = false;
        }
        else {
            this.disableemail = true;
            this.emailrequire = false;
        }
    };
    PrechatPage.prototype.numbervalue = function (val) {
        if (val == true) {
            this.disablenumber = false;
        }
        else {
            this.disablenumber = true;
            this.phonerequire = false;
        }
    };
    PrechatPage.prototype.saveform = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Please wait...',
        });
        loader.present();
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('chatfirsttoken'));
            var body = JSON.stringify({
                userId: localStorage.getItem('chatfirstid'),
                name: 1,
                nameRequired: _this.namerequire,
                email: _this.emailcheck,
                emailRequired: _this.emailrequire,
                phone: _this.phonecheck,
                phoneRequired: _this.phonerequire,
                formAutoFillRequest: _this.onlinecheck,
                formEnable: _this.prechatcheck,
                firebaseId: _this.Adminid,
            });
            console.log(body);
            _this.http.post(apiUrl + 'setup-prechat', body, { headers: headers })
                .subscribe(function (res) {
                _this.data = res.json();
                loader.dismiss();
                loader = null;
                _this.getprechat();
                console.log('pre chat data' + JSON.stringify(_this.data));
                resolve(res.json());
            }, function (err) {
                loader.dismiss();
                loader = null;
                _this.errdata = err.json();
                console.log('err' + JSON.stringify(err));
                if (_this.errdata.error == 'token_invalid') {
                    _this.events.publish('user:session');
                }
            });
        });
    };
    PrechatPage.prototype.chatbutton = function () {
        this.events.publish('user:chat');
    };
    PrechatPage.prototype.cancelform = function () {
        this.navCtrl.push(MyqueuePage);
    };
    return PrechatPage;
}());
PrechatPage = __decorate([
    IonicPage(),
    Component({
        selector: 'page-prechat',
        templateUrl: 'prechat.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, LoadingController, CommondataProvider, Http, Events])
], PrechatPage);
export { PrechatPage };
//# sourceMappingURL=prechat.js.map